# Technical Rider XDMX @ MIDIM

Check out the following to get XDMX lights installed properly on stage.

![Image Missing!](midimstage.jpg)

The XDMX stage lights installation takes round about 30 minutes.

## Stage Setup

*What we bring with us:*

### Equipment

- 2x LED RAR Lights
- 2x PAR Light Screens
- 2|3x LED Pyramides
- 1x LED Stripe
- 1x XDMX Controller
- 1x WiFi Router

*The following we need at the location  ..*

### Power

- 2x 230V Power Socket: LED PAR - Left & Right (besides the artist)
- 1x 230V Power Socket: LED Stripe - Front (@ artist desk)

Note: All lights are 5V USB powered LEDs. Issues with humming are not expected.

### Space

- 2x rectangular LED PAR Light Screen ~ 80x50x200 [cm]
    - left and right behind artist (besides vj projection)
    - the height of the areas is variable, always fits
- 2|3x LED Pyramides ~ 50x50x120 [cm]
    - in front|besides|before the artist desk
- Let us know the dimension of the stage [WxLxH] in advance
    - Nice to have: A picture of the stage

Note: XDMX Controller and WiFi router will be placed on artist desk or stage floor.

### Audio

To trigger lights by artist audio during performance.

- 3,5mm | 1/8" male stereo audio jack with artist signal @ artist desk
    - Prepare to use a stage monitor channel for this (e.g drum & bass)
    - Avoid clipping & limiters - send steady loudness audio signal

Otherwise: We bring a lavalier microphone to be applied @ the artist stage monitor or mains.

## Stage Lights

*The following gear would pimp the perfromance ..*

- A dimmable spot or moving head highlighting the artist
- A stage hazer which is controllable at front of house
- Nice to have: A big rotating mirror ball :)
    - If there is, let us know in advance

It must be dark. All other stage lights shall be switched off during performance!

Note: Venue lights may|could be used by prior consultation.

## Front of House

### Equipment

*What we bring with us:*

- A laptop and
- Some midi controllers

*The following we need at the location ..*

### Power

- 1x 230V Power Socket: For laptop usage

### Space

- Round about 1 sqm desk space (with plan view to stage)
- Nice to have: An optional seat with backrest

### Network

Lights will be controlled by our own 2.4 GHz WiFi network.

For crowded venues with FoH stage distance > 15m
  - A CAT5 ethernet cable to the stage is prefered
  - Otherwise: Keep in touch with us in advance.
