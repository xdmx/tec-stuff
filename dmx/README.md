# DMX Stuff

Here I list some DMX related lightning parts ..

## Grand MA - WLED Fixture

To be able to control WLED devices using professional [MA](https://www.malighting.com) consoles via DMX (sACN/E1.31 or ArtNet) I created the [Aircookie@WLED@Effect_Segment_White@v1.0.0_WLED-0.14.0-b1.xml](https://fixtureshare.malighting.com/share.php?page=home&manu=Aircookie&fix=WLED&mode=Effect%20Segment%20White&rev=49124) fixture file. Feel free to use it.
To further improve or to edit it, use the ma fixture share/editor service.