# XDMX Technical Information

Here you'll find technical information and stage setups I use for
XDMX music & light shows.

## Abstract

### XDMX

[XDMX](http://xamblak.de/xdmx.html) is my personal brand & DIY software DMX controller based on raspberry PI.

The controller is designed to trigger DMX fixtures via OSC, MIDI, MQTT & Audio.

It's a [docker](https://www.docker.com) application based on [OLA](https://www.openlighting.org/ola/).
For details, get in touch with me ..

### LED Stuff

I have build a lot of DIY lights using WiFi [ESP32](https://en.wikipedia.org/wiki/ESP32) microcontrollers
and [WLED](https://github.com/atuline/WLED).

For some installations I use audio reactive WLED and/or [ledFX](https://www.ledfx.app) only.

Furthermore I use ESP32 conrollers to send and receive DMX signals.

### MIDIM

Electro live music & visuals act with XDMX lights automated and triggered by VJ software.

For stage installation instructions check out: [XDMX @ MIDIM TecRider](midim/README.md)

> Note: Music & Visuals by [Michael Marquardt](http://www.michael-marquardt.com/private.html)

## References

All shows and technics are kind of differnet. But every installation includes my personal DIY lights.

Sometimes lights are triggered via (X)DMX/consoles, sometimes via LED controllers and audio only.


### 2021

#### Full June: Premiere @ [Zucker](http://zuckersachen.de) (Darmstadt)

* [Distanzunterricht](https://mxklb.github.io/sugar/) - A Shopping Window Installation

#### 24. July: Tikki Tukan Bar (Eberstadt)

* Full Moon Party - Privat Garden DJ happening

#### 21. August: [Osthang](https://www.osthang.de) (Darmstadt)

* [Cassiopeia](https://www.facebook.com/cassiopeiafestival/): Down Tempo DJ Event

#### 12. November: [Oettinger Villa](https://www.oetingervilla.de) (Darmstadt)

* [The Bad Sugar Rush](https://www.facebook.com/badsugarrush) - Album Release Concert


### 2022

#### 09. June: [Kulturwiese](https://www.facebook.com/people/Kulturwiese-Da/100071494657127/) (Darmstadt)

* Cosmic Encounters - DJ & Live Music Event

#### 21. July: 4 &deg; Fest (Bodensee)

* Barnyard Festival - Private Live Music Event

#### 05. August: [Tropentango](https://www.tropen-tango.de/) (Wollmerschied)

* [MIDIM](https://www.youtube.com/user/walters0bchak) Electro Live Music & Visuals Act

#### 19. August: [Rama Tonstudio](https://www.facebook.com/RamaSound) (Mannheim)

* [Lucid Void](https://lucidvoid.bandcamp.com) - Music Video: [Himmelheber](https://www.youtube.com/watch?v=dwXL4ySRDK4)

#### 08. October: [Bessunger Knabenschule](https://www.knabenschule.de) (Darmstadt)

* [Passage](https://musiquedepassage.org) - Psychedelic Club Night

#### 09. December: WG Party (Arheilgen)

* Private Party - DJ Event


### 2023

#### 06. January: [806 QM](https://806qm.de) (Darmstadt)
* QM Team Weihnachtsfeier - _[LedFx](https://www.ledfx.app) & DJ_

#### 07. January: [Bessunger Knabenschule](https://www.knabenschule.de) (Darmstadt)

* [Passage](https://musiquedepassage.org) - Psychedelic Club Night

#### 13. January: [Glasbläserei](https://soundcloud.com/alteglasi) (Darmstadt)

* Turnin' Thirty, stayin' dirrrty - B Day Party

#### 05. April: [Schlosskeller](https://www.schlosskeller-darmstadt.de) (Darmstadt)

* [Miasma](https://www.schlosskeller-darmstadt.de/event/23) - Techno Party

#### 06. May: [Bessunger Knabenschule](https://www.knabenschule.de) (Darmstadt)

* [Passage](https://musiquedepassage.org) - Psychedelic Club Night

#### 30. May: Tukan Tiki Bar (Eberstadt)

* Private Outdoor Full Moon Party

#### 07. June: [Schlosskeller](https://www.schlosskeller-darmstadt.de) (Darmstadt)

* [ZtudfE](https://ztudfe.bandcamp.com/album/zimtthinks-und-der-faradayische-elefant) - Album Release Concert & [Video](https://youtu.be/BeqpJ3buK7M?si=aDDUl52Ut7SVUBO8)

#### 10. June: [806 QM](https://806qm.de) (Darmstadt)

* [Hallengeburtstag](https://806qm.de/v/hallengeburtstag-kid-simius-live-sarah-wild-aka-nina-josephine-wedekind/) 20 Years QM - DMX Special

#### 08. July: [Schlosskeller](https://www.schlosskeller-darmstadt.de) (Darmstadt)

* [Miasma](https://ra.co/events/1729438) - Techno Party

#### 21-22. July: [Ruds Festival](https://ruds-festival.jimdosite.com) (Stettbach)

* Hofgut Festival - Private Music Event

#### 28-29. July: 40 Fest (Bodensee)

* Barnyard Festival - Private Live Music Event

#### 15. September: [Glasbläserei](https://soundcloud.com/alteglasi) (Darmstadt)

* Live Music: [Masima](http://masima.rocks) & Spatzen - XDMX Homebase

#### 02. October: [221 QM](https://806qm.de) (Darmstadt)

* Team Event - Ambient Light Installation

#### 07. October: Dark Gallery (Darmstadt)

* Party & Haunted House - Light Art Installation

#### 07. October: [Bessunger Knabenschule](https://www.knabenschule.de) (Darmstadt)

* [Passage](https://musiquedepassage.org) - Psychedelic Club Night

#### 22. December: Ziggy (Darmstadt)

* [Contrast Festival](https://contrastfestival.org/event/contrastfestival-off-season) (Off Season) - Techno

#### 31. December: NYE Party (Darmstadt)

* Private Party Happening


### 2024

#### 20. January: [Bessunger Knabenschule](https://www.knabenschule.de) (Darmstadt)

* [Passage](https://musiquedepassage.org) - Psychedelic Club Night

#### 06. April: [806 QM](https://806qm.de) (Darmstadt)

* [Suuupæ Slow Sessions](https://806qm.de/v/suuupae-slow-sessions-w-phae-leoleo-rik-anhalt-marcel-puntheller/) - Downtempo Party

#### 20. April: [806 QM](https://806qm.de) (Darmstadt)

* [Intergalactic Discotheque](https://806qm.de/v/intergalactic-discotheque-w-disco-sour-taha-b2b-saha/) - [House Techno](https://heineraudio.bandcamp.com)

#### 18-20. July: Tula Festival (Odenwald)

* [Gesäßhof](https://wasjetzt-odenwald.de/gesaesshof-im-odenwald/) - Private Live Music & DJ Festival 

#### 08-11. August: [Andere Welt Festival](https://anderewelt-festival.de) (Kassel)

* [Gut Kragenhof](https://gutkragenhof.de) - Electronic Music & Workshop Festival 

#### 24. August: [42* Open Air](https://42star.de) (Darmstadt)

* [Kulturwiese](https://www.facebook.com/people/Kulturwiese-Da/100071494657127/) - Live Music & Art Festival 

#### 30-31. August: [Figuren Theather Tage](https://figurentheatertage-darmstadt.de/startseite) (Darmstadt)

* [Kulturwiese](https://www.facebook.com/people/Kulturwiese-Da/100071494657127/) - Theather Festival

#### 01. December: [806 QM](https://806qm.de) (Darmstadt)

* Flowting Flux - Ambient Chill Out Event

#### 13. December: [806 QM](https://806qm.de) (Darmstadt)

* [Intergalactic Discotheque](https://806qm.de/v/intergalactic-discotheque-4/) - [House Techno](https://heineraudio.bandcamp.com)


### 2025

#### 04. January: [806 QM](https://806qm.de) (Darmstadt)
* QM Team Weihnachtsfeier - Ambient Lights

#### 05. January: [806 QM](https://806qm.de) (Darmstadt)
* Flowting Flux - Ambient Chill Out Event

#### 18. January:  [Bessunger Knabenschule](https://www.knabenschule.de) (Darmstadt)
* [42* Party](https://42star.de) - DJ Dance Event
